package module7;
public class ThreadDemo extends Thread {
    public static void main(String[] args) {
        ThreadDemo demo = new ThreadDemo();
        demo.start();
        System.out.println("this code running outside thread");
    }

    @Override
    public void run() {
        System.out.println("this code running in thread");
    }
}
