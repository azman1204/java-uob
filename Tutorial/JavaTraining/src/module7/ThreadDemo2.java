package module7;

public class ThreadDemo2 implements Runnable {
    int no;
    public static void main(String[] args) {
        for(int i=0; i<5; i++) {
            ThreadDemo2 demo = new ThreadDemo2(i);
            Thread thread = new Thread(demo);
            thread.start();
        }
    }

    ThreadDemo2(int no) {
        this.no = no;
    }

    @Override
    public void run() {
        for(int i=0; i<10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("This code run inside thread " + no);
        }
    }
}
