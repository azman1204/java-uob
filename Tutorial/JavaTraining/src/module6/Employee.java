package module6;
import java.io.Serializable;

public class Employee implements Serializable {
    private String name;
    private int age;
    Employee() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
