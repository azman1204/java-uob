package module6;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class REDemo {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("school", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher("visit my school!");
        boolean matchFound = matcher.find();
        if (matchFound) {
            System.out.println("Match found");
        } else {
            System.out.println("Not match");
        }

        String ic = "123456-12-1234"; //not valid
//        String ic = "123456x-12-1234"; // valid
        Pattern pattern2 = Pattern.compile("[0-9]{6}-[0-9]{2}-[0-9]{4}");
        Matcher matcher2 = pattern2.matcher(ic);
        boolean matchFound2 = matcher2.find();
        if (matchFound2) {
            Pattern pattern3 = Pattern.compile("-");
            String[] result = pattern3.split(ic);
            System.out.println(Arrays.toString(result));
            System.out.println("IC Valid");
        } else {
            System.out.println("IC not valid");
        }

        String email = "azman1204@yahoocom";
        Pattern pattern4 = Pattern.compile("[a-zA-Z0-9]+@[a-z]+\\.com");
        Matcher matcher4 = pattern4.matcher(email);
        if (matcher4.find()) {
            System.out.println("Email valid");
        } else {
            System.out.println("Email not valid");
        }
    }
}
