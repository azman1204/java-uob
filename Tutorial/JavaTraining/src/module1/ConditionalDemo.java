package module1;

public class ConditionalDemo {
    public static void main(String[] args) {
        // ternary operator
        boolean isAllowed = false;
        String str = isAllowed ? "Yes, you are allowed" : "You are not allowed";
        System.out.println(str);

        if (isAllowed) {
            str = "Yes, you are allowed";
        } else {
            str = "You are not allowed";
        }

        int age = 10;
        String level = ""; // empty string
        String job; // null (danger!)
        // && = AND || = OR
        if (age < 18 && age > 7) {
            // 8 - 17
            level = "teenager";
        } else if (age >= 18 && age < 50) {
            // 18 - 49
            level = "adult";
        } else if (age >= 50) {
            // 50 ...
            level = "old";
        }

        System.out.println("Level = " + level);

        String name = "M";
        switch(name) {
            case "M":
                System.out.println("Mother");
                break;
            case "F":
                System.out.println("Father");
                break;
            default:
                System.out.println("Others");
        }

        if (! name.equals("F")) {
            System.out.println("Not a father");
        }
    }
}
