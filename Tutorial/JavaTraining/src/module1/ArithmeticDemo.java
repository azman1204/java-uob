package module1;

public class ArithmeticDemo {
    public static void main(String[] args) {
        // add, minus, divide, multiply, modules
        int a = 10 + 2;
        int b = 10 - 2;
        float c = 100 / 3.0f; // auto casting (int to float)
        int d = 10 % 3; // reminder
        System.out.printf("a = %d, b = %d, c = %f, d = %d", a, b, c, d);

        // b++ = execute on the next line
        // ++b = execute on the same line
        a++;
        System.out.println("a = " + a);
        ++b;
        System.out.println("b = " + b);
        System.out.println("b = " + b++);
    }
}
