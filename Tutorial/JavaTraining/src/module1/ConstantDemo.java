package module1;

import com.sun.security.jgss.GSSUtil;

public class ConstantDemo {
    public static void main(String[] args) {
        final double PI = 3.14; // this is constant
        //PI = 5.5; this is error, constant cant change the val
    }

    public final void test() {
        System.out.println("this is final method, cant override");
    }
}

class Demo2 extends ConstantDemo {
//    public final void test() {
//        // this is error, cannot override parent method
//    }
}