package module1;

import java.util.Scanner;

public class LoopDemo {
    public static void main(String[] args) {
        // for loop
        for (int i = 1; i <= 10; i++) {
            System.out.println("i = " + i);
        }

        // while loop
        int j = 0;
        while(j < 10) {
            System.out.println("j = " + j);
            j++;
        }

        // do-while loop
        String name = "";
        Scanner scanner = new Scanner(System.in);
        do {
            name = scanner.next();
            System.out.println(name);
        } while (! name.equals("quit"));

        // foreach. use with array
        String[] names = {"Abu", "Ali", "John Doe"};
        for(String nama: names) {
            System.out.println(nama);
        }
    }
}
