package module1;

import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        int no = (int) (Math.random() * 100); // casting double to int
        //System.out.println(no);
        Scanner scanner = new Scanner(System.in);
        int trialNo = 0;
        while(true) {
            System.out.println("Guess a number : ");
            int guess = scanner.nextInt();
            if (guess != no){
                if (guess > no) {
                    System.out.println("Guess lower");
                } else {
                    System.out.println("Guess Higher");
                }
            } else {
                break; // see also continue
            }
            trialNo++;
        }
        System.out.println("Great, success after " + trialNo);
    }
}
