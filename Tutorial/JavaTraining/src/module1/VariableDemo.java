package module1;

public class VariableDemo {
    // static typing
    // int, float, ... native type
    public static void main(String[] args) {
        // number bulat / whole number
        int i = 5;
        long distance = 10000000;
        byte age = 25;

        // precision num
        double salary = 5000.00;
        float pi = 3.14f;

        // char / string
        String name = "Azman";
        char firstChar = 'A';

        // boolean
        boolean is_married = true;

        System.out.printf("My name is %s", name);

        // person is a type
        Person p = new Person();
    }
}

class Person {

}
