package module11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        Stream.of(1,2,3,4,5)
                .filter((i)-> i > 3)
                .forEach(System.out::println); // return semua data yg > 3

        Stream.of("5", "3", "8", "2")
                .map((i)-> Integer.parseInt(i))
                .forEach((i) -> {
                    System.out.println(i > 3);
                });

        // sort ascending
        Stream.of(1,3,6,4,5,2).sorted().forEach((i)-> System.out.println(i));

        // sort descending
        Stream.of(1,3,6,4,5,2)
                .sorted(Comparator.reverseOrder())
                .forEach((i)-> System.out.println(i));

        // sum all the numbers
        int[] numbers = {1,2,3,4,5,6,7,8,10};
        int sum = Arrays.stream(numbers).reduce(0, (a, b) -> a + b);
        System.out.println(sum);

        // find first
        int val = Arrays.stream(numbers).findFirst().getAsInt();
        System.out.println(val);
    }
}
