package module8;

import java.sql.*;

public class DbDemo2 {
    final String DB_URL = "jdbc:mysql://localhost/java";
    final String USER = "root";
    final String PASS = "azman1204";

    public static void main(String[] args) {
        DbDemo2 demo = new DbDemo2();
        //demo.insert();
        //demo.update("123456110003", "Ali Maju");
        demo.delete("123456110003");
    }
    public Statement getStatement() {
        Statement stmt = null;
        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stmt;
    }

    public void insert() {
        String sql = "INSERT INTO person(ic, name, age, salary) " +
                     "VALUES('123456110003', 'Ali', 35, 5500.00)";
        Statement stmt = getStatement();
        try {
            stmt.execute(sql);
            System.out.println("Successfully insert data");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(String ic, String name) {
        String sql = "UPDATE person SET name = '" + name +"' WHERE ic = '" + ic +"'";
        Statement stmt = getStatement();
        try {
            stmt.execute(sql);
            System.out.println("Successfully update data");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(String ic) {
        String sql = "DELETE FROM person WHERE ic = " + ic;
        Statement stmt = getStatement();
        try {
            stmt.execute(sql);
            System.out.println("Successfully delete data");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
