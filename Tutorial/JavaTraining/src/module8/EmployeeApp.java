package module8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.sql.ResultSet;

public class EmployeeApp {
    Statement stmt = null;
    Connection conn = null;
    final String DB_URL = "jdbc:mysql://localhost/java";
    final String USER = "root";
    final String PASS = "azman1204";
    public static void main(String[] args) {
        EmployeeApp employeeApp = new EmployeeApp();
        employeeApp.connect();
        employeeApp.start();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            mainMenu();
            System.out.print(">");
            String cmd = scanner.next();
            if (cmd.equals("5")) {
                exit();
                break;
            } else if(cmd.equals("1")) {
                listAll();
            } else if(cmd.equals("2")) {
                insert();
            } else if(cmd.equals("3")) {
                update();
            } else if(cmd.equals("4")) {
                delete();
            }
        }
    }

    void delete() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("IC : ");
        String ic = scanner.next();
        String sql = "DELETE FROM person WHERE ic = '" + ic +"'";
        try {
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("IC : ");
        String ic = scanner.next();
        System.out.println("Name : ");
        String name = scanner.next();
        System.out.println("Age : ");
        int age = scanner.nextInt();
        System.out.println("Salary");
        float salary = scanner.nextFloat();

        String sql = "UPDATE person SET ";
        if (! name.equals("-")) {
            sql += " name = '" + name + "'";
        }

        if (age != -1) {
            sql += ",age = " +age;
        }

        if (salary != -1) {
            sql += ",salary = " + salary;
        }

        sql += " WHERE ic = '" + ic + "'";
        try {
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void insert() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("IC : ");
        String ic = scanner.next();
        System.out.print("Name : ");
        String name = scanner.next();
        System.out.print("Age : ");
        int age = scanner.nextInt();
        System.out.print("Salary : ");
        float salary = scanner.nextFloat();
        String sql = String.format("INSERT INTO person(ic, name, age, salary) VALUES('%s', '%s', %d, %f)", ic, name, age, salary);
        try {
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void exit() {
        try {
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    void listAll() {
       String title = "List of all employees";
        System.out.println(title);
        System.out.println(String.format("%" + title.length() + "s", "").replace(" ", "-"));
        String sql = "SELECT * FROM person";
        List<Employee> employees = new ArrayList<>();
        try {
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Employee emp = new Employee();
                emp.setName(rs.getString("name"));
                emp.setAge(rs.getInt("age"));
                emp.setSalary(rs.getFloat("salary"));
                employees.add(emp);
            }
            //System.out.println(employees);
            String header = "No \t Name \t\t\t Age \t Salary";
            System.out.println(header);
            String line = String.format("%" + (header.length() + 32) + "s", "-").replace(" ", "-");
            System.out.println(line);
            int no = 1;
            for(Employee e: employees) {
                System.out.printf("%d \t %s \t\t\t %d \t %.2f \n", no++, e.getName(), e.getAge(), e.getSalary());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void mainMenu() {
        System.out.println("Please choose on of these");
        System.out.println("1. List all employees");
        System.out.println("2. Insert an employees");
        System.out.println("3. Update an employees");
        System.out.println("4. Delete an employees");
        System.out.println("5. Quit");
    }

    public void connect() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
