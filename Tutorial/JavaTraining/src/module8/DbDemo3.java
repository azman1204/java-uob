package module8;

import java.sql.*;

public class DbDemo3 {
    PreparedStatement stmt = null;
    Connection conn = null;
    final String DB_URL = "jdbc:mysql://localhost/java";
    final String USER = "root";
    final String PASS = "azman1204";
    public static void main(String[] args) {
        new DbDemo3().update();
    }

    public void update() {
        String sql = "UPDATE person SET name = ? WHERE ic = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Ronaldo");
            stmt.setString(2, "123456129999");
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    DbDemo3() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
