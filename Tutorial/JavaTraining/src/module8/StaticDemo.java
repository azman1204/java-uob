package module8;
// static can be applied to :
// 1. variable, 2. method, 3. class
public class StaticDemo {
    public static int count = 0;
    public static void add(int no1, int no2) {
        int tot = no1 + no2;
        System.out.println("total = " + tot);
    }

    StaticDemo() {
        count++;
    }
}

class StaticDemo2 {
    public String total = "test";
    public static void main(String[] args) {
        StaticDemo.add(4, 3);
        StaticDemo sd1 = new StaticDemo();
        StaticDemo sd2 = new StaticDemo();
        System.out.println("bil obj = " + StaticDemo.count); // biasa dipakai
        System.out.println("bil obj2 = " + sd1.count); // jarang dipakai
    }

    static class InternalClass {
        InternalClass() {
            // cannot access parent class properties
        }
    }

    class InternalClass2 {
        InternalClass2() {
            total = "2";
            Math.random();
        }
    }
}