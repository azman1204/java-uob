package module12;

import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ImageProcessor;
import java.awt.*;

public class ImageDemo {
    public static void main(String[] args) {
        ImagePlus imagePlus = IJ.openImage("assets/mouse.jpg");
        ImageProcessor ip = imagePlus.getProcessor();
        ip.setColor(Color.red);
        ip.setLineWidth(4);
        ip.drawRect(50, 50, imagePlus.getWidth() - 100, imagePlus.getHeight() - 100);
        FileSaver fs = new FileSaver(imagePlus);
        fs.saveAsJpeg("assets/mouse2.jpeg");
    }
}
