package module9;

public class LambdaDemo {
    public static void main(String[] args) {
        Message msg = () -> System.out.println("Hello Lambda");
        msg.message();

        Alert alert = (str) -> {
            String s = "You are in " + str;
            return s;
        };
        System.out.println(alert.alert("Danger!!"));
    }
}

@FunctionalInterface
interface Message {
    public void message();
}

@FunctionalInterface
interface Alert {
    public String alert(String str);
}
