package module9;

//public class GenericDemo<T> {
//public class GenericDemo<T extends Number> {
public class GenericDemo<T> {
    private T[] items = (T[]) new Object[10];
    private int count; // default = 0

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    public static void main(String[] args) {
        var list = new GenericDemo<Integer>();
        list.add(10);
        list.add(25);
        System.out.println(list.get(1));

        var list2 = new GenericDemo<String>();
        list2.add("First");
        list2.add("Second");
        System.out.println(list2.get(1));
    }
}
