package module2;
public class InnerClassDemo {
    // inner class
    private class Container {
        public void print() {
            System.out.println("This is an inner class");
        }
    }

    void printInner() {
        Container c = new Container();
        c.print();
    }

    public static void main(String[] args) {
        new InnerClassDemo().printInner();
    }
}
