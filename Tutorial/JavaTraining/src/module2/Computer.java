package module2;

public class Computer {
    // fields / variables / properties
    double cpuSpeed; // in Ghz

    // constructor is a method
    Computer() {
        cpuSpeed = 0;
        //this.cpuSpeed = 0;
    }

    Computer(double speed) {
        cpuSpeed = speed;
    }

    // method / function
    void setCpuSpeed(double _cpuSpeed) {
        cpuSpeed = _cpuSpeed;
    }

    // special name / overrides parent method
    // auto run semasa print object
    public String toString() {
        return "CPU Speed = " + cpuSpeed;
    }
}

class StartHere {
    public static void main(String[] args) {
        Computer computer = new Computer(); // instantiate/create object
        computer.setCpuSpeed(2.4);
        System.out.println(computer);

        Computer computer2 = new Computer(3.0);
        System.out.println(computer2);
    }
}