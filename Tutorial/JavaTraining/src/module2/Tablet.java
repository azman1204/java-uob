package module2;

public class Tablet implements Device {
    @Override
    public String getDeviceType() {
        return "Tablet Samsung ABC";
    }

    /**
     * This is the speed of the device
     */
    @Override
    public String getSpeed() {
        return "2.5Ghz";
    }

    public static void main(String[] args) {
        Tablet tablet = new Tablet();
        System.out.println(
                tablet.getDeviceType() +
                tablet.getSpeed());
    }
}
