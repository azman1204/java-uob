package module2;

import java.util.ArrayList;

public class ArrayDemo {
    public static void main(String[] args) {
        String[] names = {"John Doe", "Ali"};
        String[] jobs = new String[2];
        jobs[0] = "Programer";
        jobs[1] = "Designer";
        //jobs[2] = "DBA"; // error ArraiIndexOutofBoundException
//        System.out.println(jobs[1]);
        System.out.println(jobs);

        int[] no = new int[2];

        ArrayList<String> countries = new ArrayList<>();
        countries.add("Malaysia");
        countries.add("Indonesia");
        System.out.println(countries);
        countries.forEach(name -> System.out.println(name));

        // array 2 dimension
        int[][] no2 = new int[2][2];
        no2[0][0] = 1;
        no2[0][1] = 2;
        no2[1][0] = 3;
        no2[1][1] = 4;
        System.out.println(no2[1][1]);
    }
}
