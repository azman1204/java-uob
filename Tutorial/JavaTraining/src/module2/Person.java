package module2;

import com.sun.security.jgss.GSSUtil;

public class Person {
    public String name;
    byte age;
    char gender = 'M';
    boolean isMarried = true;

    public void run() {
        System.out.println(name + " is running");
    }

    public void eat() {
        System.out.println(name + " is eating");
    }

    private void iqLevel() {
        System.out.println("1000 IQ");
    }

    @Override
    public String toString() {
        return "Name = " + name;
    }
}
