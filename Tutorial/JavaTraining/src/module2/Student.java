package module2;
public class Student extends Person {
    String matrixNo;
    float cgpa;

    Student(String matrixNo, float cgpa) {
        this.matrixNo = matrixNo;
        this.cgpa = cgpa;
    }

    public String toString() {
        return "Name = " + name + "age = " + age + " matrix no = " + matrixNo;
    }

    public static void main(String[] args) {
        Student stu = new Student("1234", 3.5f);
        stu.name = "John Doe";
        stu.age = 21;
        stu.isMarried = false;
        //stu.iqLevel(); error, private method cannot inherit
        System.out.println(stu);

        Person p = new Student("xx", 4);
        System.out.println(p);
    }
}
