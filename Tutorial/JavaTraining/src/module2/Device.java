package module2;
public interface Device {
    public String getDeviceType();
    public String getSpeed();
}
