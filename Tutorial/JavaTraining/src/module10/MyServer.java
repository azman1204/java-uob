package module10;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(7000);
            Socket socket = ss.accept(); // establish connection
            DataInputStream input = new DataInputStream(socket.getInputStream());
            String str = (String) input.readUTF();
            System.out.println("input : " + str);
            socket.close();
            ss.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
