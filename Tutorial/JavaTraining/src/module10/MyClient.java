package module10;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MyClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 7000);
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF("Hello World...");
            out.flush();
            out.close();
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
