package module3;

import java.util.Scanner;

public class InvestmentCalculator {
    public static void main(String[] args) {
        // input from user: 1. amount, 2. duration(years), 3. return rate (%)
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter investment amount : ");
        int amount = scanner.nextInt(); // 100_000
        System.out.println("Enter investment duration : ");
        int years = scanner.nextInt(); // 10
        System.out.println("Enter return rate : ");
        float rate = scanner.nextFloat(); // 5.5%

        float total = amount;
        System.out.println("Year \t Amount \t Return \t Total");
        for(int i=0; i<years; i++) {
            // profit = investment * rate
            float profit = total * rate/100;
            //total = total + profit;
            float subTotal = total;
            total += profit;
            System.out.printf("%d \t %.2f \t %.2f \t %.2f \n", i+1, subTotal, profit, total);
        }
    }
}
