package module3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayListDemo {
    public static void main(String[] args) {
        // collection must be data type of Object (no primitive)
        List<Integer> list = new ArrayList<>();
        list.add(35);
        list.add(19);
        list.add(11);
        list.add(83);
        list.add(19);
        list.add(7);
        int index = list.indexOf(19);// search data
        System.out.println(index);
        System.out.println(list.get(index));
        // loop using lambda
        list.forEach(num -> System.out.println(num));

        for(Integer no: list) {
            System.out.println("no = " + no);
        }

        for(int i=0; i<list.size(); i++) {
            System.out.println("num = " + list.get(i));
        }

        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()) {
            System.out.println("data = " + iterator.next());
        }
    }
}
