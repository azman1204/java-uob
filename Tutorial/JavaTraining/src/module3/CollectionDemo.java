package module3;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CollectionDemo {
    public static void main(String[] args) {
        // set is ordered list
        // set cant have duplicate value. auto remove if duplicate
        Integer[] myArray = new Integer[] {3, 25, 2, 79, 2};
        Set<Integer> mySet = new HashSet<>(Arrays.asList(myArray));
        mySet.add(10);
        System.out.println(mySet);
        System.out.println("set size = " + mySet.size());

        // create empty set
        Set<String> nameSet = new HashSet<>();
        nameSet.add("John Doe");
        nameSet.add("Azman");

        String[] names = {"Abu", "John"};
        Collections.addAll(nameSet, names);
        System.out.println(nameSet);
    }
}
