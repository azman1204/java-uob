package module3;

import module2.Person;

import java.util.HashSet;
import java.util.Set;

public class CollectionDemo2 {
    public static void main(String[] args) {
        Set<Person> persons = new HashSet<>();

        Person p1 = new Person();
        p1.name = "John Doe";
        persons.add(p1);

        Person p2 = new Person();
        p2.name = "Abu";
        persons.add(p2);

        System.out.println(persons);
    }
}
