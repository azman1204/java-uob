package module3;

import java.util.ArrayList;
import java.util.Arrays;

public class InvestmentCalculator2 {
    public static void main(String[] args) {
        ArrayList<Investor> investors = new ArrayList<>();
        investors.add(new Investor("John Doe", 100_000, 10));
        investors.add(new Investor("Abu", 200_000, 20));
        investors.add(new Investor("Rama Samy", 300_000, 30));

        int maxYears = 0;
        for(Investor investor: investors) {
            if (investor.years > maxYears) maxYears = investor.years;
        }

        int[] returnRate = new int[maxYears];
        for(int i=1; i<=maxYears; i++) {
            int rand = (int) (Math.random() * 15); // 0 to 15
            returnRate[i-1] = rand;
        }
        System.out.println(Arrays.toString(returnRate)); // [2, 3, 7, ...]

        for(Investor investor: investors) {
            int total = investor.amount;
            System.out.println("NAME : " + investor.name);
            System.out.println("No \t Amount \t Profit \t Total");
            for(int i=1; i<=investor.years; i++) {
                int profit = total * returnRate[i - 1] / 100;
                int subTotal = total;
                total += profit;
                System.out.printf("%d \t %d \t %d \t %d \n", i, subTotal, profit, total);
            }
        }
    }
}

class Investor {
    public String name; // investor name
    public int amount; // initial investment amount
    public int years; // duration of investment

    Investor(String name, int amount, int years) {
        this.name = name;
        this.amount = amount;
        this.years = years;
    }
}