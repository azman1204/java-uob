package module3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class InvestmentCalculator3 {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Investor> investors = new ArrayList<>();
        investors.add(new Investor("John Doe", 100_000, 10));
        investors.add(new Investor("Abu", 200_000, 20));
        investors.add(new Investor("Rama Samy", 300_000, 30));

        int maxYears = 0;
        for(Investor investor: investors) {
            if (investor.years > maxYears) maxYears = investor.years;
        }

        int[] returnRate = new int[maxYears];
        for(int i=1; i<=maxYears; i++) {
            int rand = (int) (Math.random() * 15); // 0 to 15
            returnRate[i-1] = rand;
        }
        System.out.println(Arrays.toString(returnRate)); // [2, 3, 7, ...]

        for(Investor investor: investors) {
            int total = investor.amount;
            String out = "";
            out += "NAME : " + investor.name + "\n";
            out += "No \t Amount \t Profit \t Total \n";

            for(int i=1; i<=investor.years; i++) {
                int profit = total * returnRate[i - 1] / 100;
                int subTotal = total;
                total += profit;
                out += i + "\t" + subTotal + "\t" + profit + "\t" + total + "\n";
            }

            // write result to file here
            try {
                FileWriter writer = new FileWriter(investor.name + ".txt");
                writer.write(out);
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
