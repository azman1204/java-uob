package module3;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("salary", 5000);
        map.put("age", 40);
        System.out.println(map);
        Integer age = map.get("age");
        System.out.println("age = " + age);

        Set<String> keys = map.keySet();
        System.out.println(keys);
        for(String key: keys) {
            Integer val = map.get(key);
            System.out.println("val = " + val);
        }
    }
}
