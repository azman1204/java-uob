package module4;

import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
/**
 * a program just like DOS program which support the following command
 * 1. ls, mkdir, del, cd, exit
 * i.e ls C:\
 */
public class DosCommand {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String dir = "C:\\temp";
        while(true) {
            System.out.printf("me (%s)  > ", dir);
            String str = scanner.nextLine();
            String[] commands = str.split(" "); // [ls] [cd, c:\\users], [mkdir, test], [rm, test.txt]
            System.out.println(Arrays.toString(commands));
            String cmd = commands[0];
            if (cmd.equals("ls")) {
                File file = new File(dir);
                File[] files = file.listFiles();
                for(File f: files) {
                    System.out.println(f.getName());
                }
            } else if (cmd.equals("exit")) {
                break;
            } else if (cmd.equals("cd")) {
                dir = commands[1]; // cd c:\\Users
            } else if (cmd.equals("mkdir")) {
                File f = new File(dir + "\\" + commands[1]); // C:\\temp\\test
                f.mkdir();
            } else if (cmd.equals("rm")) {
                File f = new File(dir + "\\" + commands[1]); // C:\\temp\test.txt;
                f.delete();
            } else if (cmd.equals("search")) {
                File f = new File(dir);
                File[] files = f.listFiles();
                for(File file: files) {
                    if (file.getName().contains(commands[1]))
                        System.out.println(file.getName());
                }
            } else {
                System.out.println("command does not exist");
            }
        }
    }
}
