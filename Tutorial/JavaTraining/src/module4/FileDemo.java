package module4;

import java.io.File;

/**
 * 1. list files and folder
 * 2. create folder
 */
public class FileDemo {
    public static void main(String[] args) {
        // list files and folders
        File file = new File("c:\\temp");
        File[] files = file.listFiles();
        for(File f: files) {
            if (f.isFile()) {
                System.out.print("F : ");
                System.out.print(f.length());
            } else
                System.out.print("D : ");
            System.out.println(f.getName());
        }

        // create a folder
        File dir = new File("C:\\temp\\test");
        dir.mkdir();
    }
}
