package module4;
import java.io.*;
public class ReadWriteDemo {
    public static void main(String[] args) {
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream("input.txt");
            out = new FileOutputStream("out.txt");
            int content;
            while((content = in.read()) != -1) {
                out.write(content);
            }
            in.close();
            out.close();
        } catch(FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            System.out.println("Always run");
        }
    }
}
