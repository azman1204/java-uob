package module5;

import java.util.Arrays;

/**
 * String manipulation demonstration
 */
public class StringDemo {
    public static void main(String[] args) {
        String name = "John Doe";
        System.out.println(name.toUpperCase());
        String name2 = "Abu";
        String name3 = "Ali";
        System.out.println(name
                .concat(" ")
                .concat(name2)
                .concat(" ")
                .concat(name3));
        String[] data = name.split(" ");
        System.out.println(Arrays.toString(data));

        // original string is not modified
        String str = "lorem ipsum dolar sit amet";
        String str2 = str.replace('e', 'a');
        System.out.println(str);
        System.out.println(str2);
        String str3 = str.replace("sit", "siti");
        System.out.println(str3);
        // indexOf(), substring()
        int index = str.indexOf("sit");
        String str4 = str.substring(index);
        System.out.println(str4);
        String str5 = str.substring(index, index + 3);
        System.out.println(str5);
    }
}
