import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

import java.io.IOException;
import java.io.PrintWriter;

//http://localhost:8080/web/hello?name=azman
@WebServlet("/hello")
public class FirstServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter writer = response.getWriter();
            String name = request.getParameter("name"); // read input from URL / form
            writer.print("<h1>Hello World from " + name + "</h1>");
            HttpSession session = request.getSession();
            session.setAttribute("uname", name);
            // cookie
            Cookie cookie = new Cookie("user", "JohnDoe");
            response.addCookie(cookie);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
