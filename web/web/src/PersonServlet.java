import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import sample.Person;
import sample.PersonModel;


import java.io.IOException;
import java.util.ArrayList;
@WebServlet("/person-list")
public class PersonServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PersonModel person = new PersonModel();
        ArrayList<Person> persons = person.getAll();
        System.out.println(persons);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/person_list.jsp");
        req.setAttribute("persons", persons);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        float salary = Float.parseFloat(req.getParameter("salary"));
        String ic = req.getParameter("ic");
        Person p = new Person();
        p.setIc(ic);
        p.setSalary(salary);
        p.setAge(age);
        p.setName(name);
        PersonModel person = new PersonModel();
        person.store(p);

        // redirect ke person list
//        RequestDispatcher dispatcher = req.getRequestDispatcher("/person-list");
//        dispatcher.forward(req, resp);
        resp.sendRedirect(req.getContextPath() + "/person-list"); // redirect GET
    }
}
