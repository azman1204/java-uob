package sample;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import com.mysql.jdbc.Driver;

public class Mysql {
    public Connection conn;
    public Statement stmt;

    public void init() {

        String url = "jdbc:mysql://localhost/java";
        try {
            Driver driver = new Driver();
            conn = DriverManager.getConnection(url, "root", "azman1204");
            stmt = conn.createStatement();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
