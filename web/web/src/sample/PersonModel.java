package sample;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PersonModel {
    public ArrayList<Person> getAll() {
        Mysql mysql = new Mysql();
        mysql.init();
        String sql = "SELECT * FROM person";
        ArrayList<Person> persons = new ArrayList<>();
        try {
            ResultSet rs = mysql.stmt.executeQuery(sql);
            while(rs.next()) {
                Person person = new Person();
                person.setName(rs.getString("name"));
                person.setAge(rs.getInt("age"));
                person.setSalary(rs.getFloat("salary"));
                person.setIc(rs.getString("ic"));
                persons.add(person);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return persons;
    }

    public Person getOne(String ic) {
        return null;
    }

    public boolean store(Person person) {
        String sql = "INSERT INTO person (name, ic, age, salary) VALUES" +
                     "('"+ person.getName() +"', '" + person.getIc() + "'," +
                     person.getAge() + "," + person.getSalary() + ")";
        Mysql mysql = new Mysql();
        mysql.init();
        try {
            mysql.stmt.execute(sql);
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean update(Person person) {
        return false;
    }

    public boolean delete(String ic) {
        return false;
    }
}

