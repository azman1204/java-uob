<%@ page import="sample.Person, java.util.ArrayList" %>

<h2>Person List</h2>

<table border="1" width="100%">
    <tr>
        <td>No</td>
        <td>Name</td>
        <td>Age</td>
        <td>Salary</td>
    </tr>
    <%
    int no = 1;
    ArrayList<Person> persons = (ArrayList<Person>) request.getAttribute("persons");
    for(Person person: persons) {
    %>
    <tr>
        <td><%= no++ %></td>
        <td><%= person.getName() %></td>
        <td><%= person.getAge() %></td>
        <td><%= person.getSalary() %></td>
    </tr>
    <% } %>
</table>