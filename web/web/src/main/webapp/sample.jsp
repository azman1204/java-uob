<%@ page import="java.io.*, java.util.*, sample.Calculator" %>
<%
String title = "Welcome back to my website";
Date today = new Date();
out.print("......");
Calculator cal = new Calculator();
String name = (String) session.getAttribute("uname");
%>

<h2><%= title %></h2>
Today : <%= today %>
<hr>
Name = <%= name %>
<br>
5 + 5 = <%= cal.add(5,5) %>
<hr>
Greeting : <%= sayHi("John Doe") %>

<%!
public String sayHi(String name) {
    return "Hi " + name;
}
%>