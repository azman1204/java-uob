

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/hello2")
public class SecondServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter writer  = response.getWriter();
            HttpSession session = request.getSession();
            String name = (String) session.getAttribute("uname");
            writer.write("<b>Hello " + name + "</b>");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
