package com.example.demo5.web;

import com.example.demo5.domain.Car;
import com.example.demo5.domain.CarRepository;
import com.example.demo5.domain.Owner;
import com.example.demo5.domain.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
public class OwnerController {
    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    CarRepository carRepository;

    @GetMapping("/owners")
    public Iterable<Owner> getAll() {
        return ownerRepository.findAll();
    }

    @GetMapping("/owner") // http://localhost:8080/owner?id=2
    public Owner getOne(@RequestParam Long id) {
        Optional<Owner> opt = ownerRepository.findById(id);
        if (opt.isPresent()) {
            return opt.get();
        } else {
            return new Owner();
        }
    }

    @GetMapping("/owner2") // http://localhost:8080/owner?id=2
    public Iterable<Car> getOne2(@RequestParam Long id) {
        Optional<Owner> opt = ownerRepository.findById(id);
        if (opt.isPresent()) {
            Owner owner = opt.get();
            List<Car> cars = carRepository.findByOwner(owner);
            //owner.setCars(cars);
            return cars;
        } else {
            return null;
        }
    }

    @PostMapping("/owner")
    public Owner insertOwner(@RequestBody Owner owner) {
        ownerRepository.save(owner);
        return owner;
    }

    @PutMapping("/owner")
    public Owner updateOwner(@RequestBody Owner owner) {
        Optional<Owner> opt = ownerRepository.findById(owner.getOwnerId());
        if (opt.isPresent()) {
            Owner own = opt.get(); // ini data dr db
            own.setFirstName(owner.getFirstName());
            own.setLastName(owner.getLastName());
            ownerRepository.save(own);
            return own;
        } else {
            return new Owner();
        }
    }

    @DeleteMapping("/owner/{ownerId}") // http://localhost:8080/owner/1
    public Owner deleteOwner(@PathVariable long ownerId) {
        Optional<Owner> opt = ownerRepository.findById(ownerId);
        if (opt.isPresent()) {
            Owner own = opt.get();
            ownerRepository.delete(own);
            return own;
        } else {
            return new Owner();
        }
    }
}
