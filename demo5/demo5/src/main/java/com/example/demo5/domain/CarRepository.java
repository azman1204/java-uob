package com.example.demo5.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepository extends CrudRepository<Car, Long> {
    // <Car, Long> = Car = component, Long = data type for pk
    // SELECT * FROM car WHERE brand = '?'
    @Query("select c from Car c where c.brand =?1")
    List<Car> findByBrand(String brand);

    List<Car> findByColor(String color);

    List<Car> findByYear(int year);

    List<Car> findByOwner(Owner owner);
}
