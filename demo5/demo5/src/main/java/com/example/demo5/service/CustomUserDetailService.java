package com.example.demo5.service;

import com.example.demo5.domain.User;
import com.example.demo5.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> opt = userRepository.findById(username);
        if (! opt.isPresent()) {
            throw new UsernameNotFoundException("user not found " + username);
        }

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        User user = opt.get();

        authorities.add(new SimpleGrantedAuthority(user.getRole()));
        return new org.springframework.security.core.userdetails.User(
            username, user.getPass(), authorities
        );
    }
}
