package com.example.demo5.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class AdminController {
    //@PreAuthorize("hasRole('admin')") ROLE_ADMIN, ROLE_EDITOR
    @GetMapping("/admin/user-list")
    @PreAuthorize("hasAuthority('admin')")
    public String userList() {
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        System.out.println(authorities);
        System.out.println(SecurityContextHolder.getContext().getAuthentication().toString());
        return "admin/user-list";
    }
}
