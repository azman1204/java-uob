package com.example.demo5.config;

import com.example.demo5.service.CustomUserDetailService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {
    // authentication
    @Bean
    public UserDetailsService userDetailsService() {
        // Spring Boot use UserDetailsService as a default auth
        // this function will override the default functionality
        return new CustomUserDetailService();
    }

    // password encode
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // bean for authentication provider
    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService());
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }

    // authorization bean
    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        // CSRF = Cross Site Request Forgery
        // / = home
        // /pub/** = /pub/list OR /pub/edit...
        return http.csrf().disable().
                authorizeHttpRequests(auth -> {
                    auth.requestMatchers("/", "/**", "/gen-pass", "/car2").permitAll();
                    //.requestMatchers("/admin/**").hasRole("admin")
                    //.anyRequest().authenticated();
                }).formLogin().and().build();
    }
}
