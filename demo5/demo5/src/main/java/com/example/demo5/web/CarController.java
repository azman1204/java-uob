package com.example.demo5.web;

import com.example.demo5.domain.Car;
import com.example.demo5.domain.CarRepository;
import com.example.demo5.domain.Owner;
import com.example.demo5.domain.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class CarController {
    @Autowired
    private CarRepository repository;

    @Autowired
    private OwnerRepository ownerRepository;

    @RequestMapping("/cars") // http://localhost:8080/cars
    public Iterable<Car> getCars() {
        return repository.findAll();
    }

    @PostMapping("/car") // http://localhost:8080?ownerId=2
    public Car insertCar(@RequestBody Car car, @RequestParam long ownerId) {
        // assumption for insert new car with existing owner
        Optional<Owner> opt = ownerRepository.findById(ownerId);
        car.setOwner(opt.get());
        repository.save(car);
        return car;
    }

    @PostMapping("/car2") // http://localhost:8080/car2
    public Car insertCar2(@RequestBody Car car) {
        // assumption for insert new car with new owner
        Owner owner = car.getOwner();
        ownerRepository.save(owner);
        car.setOwner(owner);
        repository.save(car);
        return car;
    }
}
