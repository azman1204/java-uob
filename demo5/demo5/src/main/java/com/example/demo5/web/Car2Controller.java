package com.example.demo5.web;

import com.example.demo5.domain.Car;
import com.example.demo5.domain.CarRepository;
import com.example.demo5.domain.Owner;
import com.example.demo5.domain.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;
import java.util.OptionalInt;

@Controller
public class Car2Controller {
    @Autowired
    CarRepository carRepository;

    @Autowired
    OwnerRepository ownerRepository;

    @GetMapping("/car2")
    public String listing(Model model) {
        Iterable<Car> cars = carRepository.findAll();
        model.addAttribute("cars", cars);
        return "car/listing";
    }

    @GetMapping("/car-create")
    public String create(Model model) {
        Car car = new Car();
        Owner owner = new Owner();
        car.setOwner(owner);
        System.out.println(owner.toString());
        Iterable<Owner> owners = ownerRepository.findAll();
        model.addAttribute("car", car);
        model.addAttribute("owners", owners);
        return "car/form";
    }

    @PostMapping("/car-store")
    public String store(Model model, @ModelAttribute Car data) {
        Optional<Owner> opt = ownerRepository.findById(
                data.getOwner().getOwnerId());
        Owner owner = opt.get();
        carRepository.save(data); // insert OR update
        System.out.println(data.getOwner().getOwnerId());
        return "redirect:/car2";
    }

    @GetMapping("/car-edit/{id}")
    public String edit(Model model, @PathVariable Long id) {
        Optional<Car> opt = carRepository.findById(id);
        Car car = opt.get();
        Iterable<Owner> owners = ownerRepository.findAll();
        model.addAttribute("car", car);
        model.addAttribute("owners", owners);
        return "car/form";
    }

    @GetMapping("/car-delete/{id}")
    public String delete(@PathVariable Long id) {
        Optional<Car> opt = carRepository.findById(id);
        Car car = opt.get();
        carRepository.delete(car);
        return "redirect:/car2";
    }
}
