package com.example.demo5.web;

import com.example.demo5.domain.Owner;
import com.example.demo5.domain.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.Optional;

@Controller
public class Owner2Controller {
    @Autowired
    OwnerRepository ownerRepository;

    @GetMapping("/owner3/{ownerId}")
    public String owner(Model model, @PathVariable Long ownerId) {
        // model - is used to pass data from controller to view
        Optional<Owner> opt = ownerRepository.findById(ownerId);
        Owner owner = opt.get();
        String name = "John Doe";
        model.addAttribute("owner", owner); // passing data to view
        model.addAttribute("name", name); // passing data to view
        return "owner/owner"; // resources/templates/owner.html
    }

    @GetMapping("/owner3-list")
    public String ownerList(Model model) {
        Iterable<Owner> owners = ownerRepository.findAll();
        model.addAttribute("no", 1);
        model.addAttribute("owners", owners);
        return "owner/owner_list";
    }

    @GetMapping("/owner3-create")
    public String ownerCreate(Model model) {
        return "owner/form";
    }

    @PostMapping("/owner3-save")
    public String ownerSave(@ModelAttribute Owner owner, Model model) {
        System.out.println(owner.getFirstName());
        ownerRepository.save(owner);
        return "redirect:/owner3-list";
    }

//    @GetMapping("/owner3-edit/{ownerId}")
//    public String ownerEdit(@PathVariable Long ownerId, Model model) {
    @GetMapping("/owner3-edit")
    public String ownerEdit(@RequestParam Long ownerId, Model model) {
        Optional<Owner> opt = ownerRepository.findById(ownerId);
        Owner owner = opt.get();
        System.out.println(owner.getFirstName());
        model.addAttribute("owner", owner);
        return "owner/form_edit";
    }

    @PostMapping("/owner3-update")
    public String ownerUpdate(@ModelAttribute Owner owner) {
        Optional<Owner> opt = ownerRepository.findById(owner.getOwnerId());
        Owner owner2 = opt.get();
        owner2.setFirstName(owner.getFirstName());
        owner2.setLastName(owner.getLastName());
        ownerRepository.save(owner2);
        return "redirect:/owner3-list";
    }

    @GetMapping("/owner3-delete")
    public String ownerDelete(@RequestParam Long ownerId) {
        Optional<Owner> opt = ownerRepository.findById(ownerId);
        Owner owner = opt.get();
        ownerRepository.delete(owner);
        return "redirect:/owner3-list";
    }
}
