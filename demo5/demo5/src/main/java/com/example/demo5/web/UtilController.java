package com.example.demo5.web;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UtilController {
    @GetMapping("/gen-pass")
    public String genPassword() {
        System.out.println(BCrypt.hashpw("1234", BCrypt.gensalt(5)));
        return "owner/owner_list";
    }
}
