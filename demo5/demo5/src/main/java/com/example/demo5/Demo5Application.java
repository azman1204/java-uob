package com.example.demo5;

import com.example.demo5.domain.Car;
import com.example.demo5.domain.CarRepository;
import com.example.demo5.domain.Owner;
import com.example.demo5.domain.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@SpringBootApplication
public class Demo5Application implements CommandLineRunner {
	@Autowired
	private CarRepository repository;

	@Autowired
	private OwnerRepository ownerRepository;

	public static void main(String[] args) {
		SpringApplication.run(Demo5Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Owner owner1 = new Owner("John", "Johnson");
//		Owner owner2 = new Owner("Mary", "Robinson");
//		ownerRepository.saveAll(Arrays.asList(owner1, owner2));
//
//		repository.save(new Car("Ford", "Mustang", "Red", "ADF-1121",
//				2021, 59_000, owner1));
//		repository.save(new Car("Nissan", "Leaf", "White", "SSJ-3002",
//				2019, 29_000, owner2));
//		repository.save(new Car("Toyota", "Prius", "Silver", "KKO-0112",
//				2020, 39_000, owner2));

		// list car and their owner
//		for(Car car: repository.findAll()) {
//			System.out.println(car.getBrand() + " " + car.getModel());
//			Owner owner = car.getOwner();
//			System.out.println(owner.getFirstName());
//		}

		// list all owner and their cars
		for(Owner owner: ownerRepository.findAll()) {
			System.out.println(owner.getFirstName());
			List<Car> cars = owner.getCars();
			for(Car car: cars) {
				System.out.println(car.getBrand());
			}
		}
	}
}
