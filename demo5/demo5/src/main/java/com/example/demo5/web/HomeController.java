package com.example.demo5.web;

import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String home(HttpSession sess) {
        sess.setAttribute("name", "Azman");
        return "home";
    }
}
