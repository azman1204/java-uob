create table car (
	id int not null primary key auto_increment,
    brand varchar(50),
    color varchar(50),
    model varchar(50),
    price int,
    register_number varchar(50), -- registerNumber
    year int
);

create table owner(
	owner_id int not null primary key auto_increment,
    first_name varchar(50),
    last_name varchar(50)
);

alter table car add column owner int;

delete from car where id > 0;
delete from owner where owner_id > 0;


select * from car;
select * from owner;